package net.spectre.pirates;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.spectre.pirates.blocks.ModBlocks;
import net.spectre.pirates.items.ModItems;

@Mod(Pirates.MODID)
public class Pirates {

    public static final String MODID = "pirates";


    public Pirates(){

        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        ModItems.ITEMS.register(bus);
        ModBlocks.BLOCKS.register(bus);

    }

}
