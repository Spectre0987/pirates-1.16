package net.spectre.pirates.itemtabs;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.spectre.pirates.Pirates;
import net.spectre.pirates.items.ModItems;

public class ModTabs {

    public static final ItemGroup MAIN = new ItemGroup(Pirates.MODID + ".main") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ModItems.PIRATA_CODEX.get());
        }
    };

}
