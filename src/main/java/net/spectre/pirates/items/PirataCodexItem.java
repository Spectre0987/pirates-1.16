package net.spectre.pirates.items;

import net.minecraft.item.Item;
import net.spectre.pirates.itemtabs.ModTabs;

public class PirataCodexItem extends Item {


    public PirataCodexItem() {
        super(
                new Properties().tab(ModTabs.MAIN)
                .stacksTo(1)
                .setNoRepair()
        );
    }
}
