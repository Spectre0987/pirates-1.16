package net.spectre.pirates.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.spectre.pirates.Pirates;

public class ModItems {


    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(Item.class, Pirates.MODID);

    public static final RegistryObject<Item> PIRATA_CODEX = ITEMS.register("pirata_codex", PirataCodexItem::new);

}
