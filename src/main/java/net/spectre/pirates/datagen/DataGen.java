package net.spectre.pirates.datagen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.spectre.pirates.Pirates;
import net.spectre.pirates.datagen.providers.PirateItemModelProvider;
import net.spectre.pirates.datagen.providers.PirateLangProvider;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @SubscribeEvent
    public static void onData(GatherDataEvent event){
        event.getGenerator().addProvider(new PirateLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(new PirateItemModelProvider(event.getGenerator()));
    }

}
