package net.spectre.pirates.datagen.providers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.spectre.pirates.Pirates;
import net.spectre.pirates.datagen.DataGen;
import net.spectre.pirates.items.ModItems;

import java.io.IOException;
import java.nio.file.Path;

public class PirateItemModelProvider implements IDataProvider {

    private DataGenerator generator;

    public PirateItemModelProvider(DataGenerator generator) {
        this.generator = generator;
    }

    @Override
    public void run(DirectoryCache cache) throws IOException {
        this.generateModel(ModItems.PIRATA_CODEX.get(), this.generateBasicItem(ModelType.GENERATED, "pirata_codex"), cache);
    }

    @Override
    public String getName() {
        return "Pirates ModelGenerator";
    }

    public JsonObject generateBasicItem(ModelType type, String... textures){
        JsonObject root = new JsonObject();
        root.add("parent", new JsonPrimitive("item/" + type.path));

        JsonObject texture = new JsonObject();
        for(int i = 0; i < textures.length; ++i){
            texture.add("layer" + i, new JsonPrimitive(Pirates.MODID + ":item/" + textures[i]));
        }
        root.add("textures", texture);

        return root;
    }

    public void generateModel(Item item, JsonElement model, DirectoryCache cache) throws IOException{
        IDataProvider.save(DataGen.GSON, cache, model, getPath(generator.getOutputFolder(), item));
    }

    public Path getPath(Path path, Item item){
        ResourceLocation key = item.getRegistryName();
        return path.resolve("assets/" + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
    }

    enum ModelType{
        GENERATED("generated"),
        ROD("handheld");

        public final String path;

        ModelType(String path){
            this.path = path;
        }
    }

}
