package net.spectre.pirates.datagen.providers;

import net.minecraft.data.BlockStateProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IDataProvider;

public class BlockModelProvider extends BlockStateProvider {

    public BlockModelProvider(DataGenerator p_i232520_1_) {
        super(p_i232520_1_);
    }
}
