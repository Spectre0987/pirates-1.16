package net.spectre.pirates.datagen.providers;

import net.minecraft.data.DataGenerator;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.common.data.LanguageProvider;
import net.spectre.pirates.Pirates;
import net.spectre.pirates.items.ModItems;
import net.spectre.pirates.itemtabs.ModTabs;

public class PirateLangProvider extends LanguageProvider {

    public PirateLangProvider(DataGenerator gen) {
        super(gen, Pirates.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {

        this.add("itemGroup.pirates.main", "Pirates");

        this.add(ModItems.PIRATA_CODEX.get(), "Pirata Codex");
    }
}
