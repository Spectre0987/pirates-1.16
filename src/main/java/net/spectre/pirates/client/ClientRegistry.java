package net.spectre.pirates.client;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.spectre.pirates.Pirates;
import net.spectre.pirates.blocks.ModBlocks;

@Mod.EventBusSubscriber(modid = Pirates.MODID, value = Dist.CLIENT)
public class ClientRegistry {

    @SubscribeEvent
    public static void register(FMLClientSetupEvent event){

        RenderTypeLookup.setRenderLayer(ModBlocks.CANNON.get(), RenderType.cutoutMipped());

    }

}
