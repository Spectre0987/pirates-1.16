package net.spectre.pirates.blocks;


import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.spectre.pirates.blocks.misc.DirectionalVoxelShape;

public class CannonBlock extends HorizontalBlock {

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(makeShape());

    public CannonBlock(Properties p_i48440_1_) {
        super(p_i48440_1_);
    }

    @Override
    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader p_220053_2_, BlockPos p_220053_3_, ISelectionContext p_220053_4_) {
        return SHAPE.getShape(state.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = VoxelShapes.empty();
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.09375, 0, 0, 0.15625, 0.1875, 0.1875), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.84375, 0, 0, 0.90625, 0.1875, 0.1875), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.15625, 0.0625, 0.0625, 0.84375, 0.25, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.15625, 0.25, 0.0625, 0.28125, 0.4375, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.71875, 0.25, 0.0625, 0.84375, 0.4375, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.15625, 0.4375, 0.0625, 0.28125, 0.625, 0.4375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.71875, 0.4375, 0.0625, 0.84375, 0.625, 0.4375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.3125, 0.34375, -0.125, 0.6875, 0.71875, 0.625), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.125, 0.46875, 0.3125, 0.875, 0.53125, 0.375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.0625, 0.0625, 0.0625, 0.125, 0.125, 0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.875, 0.0625, 0.0625, 0.9375, 0.125, 0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.0625, 0.0625, 0.875, 0.125, 0.125, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.875, 0.0625, 0.875, 0.9375, 0.125, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.28125, 0.6875, -0.1875, 0.71875, 0.75, -0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.28125, 0.3125, -0.1875, 0.71875, 0.375, -0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.34375, 0.375, -0.140625, 0.65625, 0.6875, -0.140625), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.65625, 0.375, -0.1875, 0.71875, 0.6875, -0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.28125, 0.375, -0.1875, 0.34375, 0.6875, -0.125), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.28125, 0.3125, 0.625, 0.71875, 0.75, 1), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.09375, 0, 0.8125, 0.15625, 0.1875, 1), IBooleanFunction.OR);
        shape = VoxelShapes.join(shape, VoxelShapes.box(0.84375, 0, 0.8125, 0.90625, 0.1875, 1), IBooleanFunction.OR);

        return shape;
    }
}
