package net.spectre.pirates.blocks.misc;

import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;

public class DirectionalVoxelShape {

    private VoxelShape north;
    private VoxelShape east;
    private VoxelShape west;
    private VoxelShape south;

    public DirectionalVoxelShape(VoxelShape shape){
        this.north = shape;
        this.east = this.rotateShape(270, shape);
        this.south = this.rotateShape(180, shape);
        this.west = this.rotateShape(90, shape);

    }

    public Vector3d rotatePoint(double angle, double oldX, double oldY, double oldZ){
        double radians = Math.toRadians(angle);

        double x = oldX * Math.cos(radians) + oldZ * Math.sin(radians);
        double z = oldZ * Math.cos(radians) - oldX * Math.sin(radians);
        return new Vector3d(x, oldY, z);
    }

    public AxisAlignedBB rotateAABB(double angle, AxisAlignedBB bb){
        return new AxisAlignedBB(this.rotatePoint(angle, bb.minX, bb.minY, bb.minZ), this.rotatePoint(angle, bb.maxX, bb.maxY, bb.maxZ));
    }

    public VoxelShape rotateShape(float angle, VoxelShape shape){
        VoxelShape newShape = VoxelShapes.empty();
        for(AxisAlignedBB bb : shape.toAabbs()){
            newShape = VoxelShapes.join(newShape, VoxelShapes.create(this.rotateAABB(angle, bb.move(-0.5, -0.5, -0.5)).move(0.5, 0.5, 0.5)), IBooleanFunction.OR);
        }
        return newShape;
    }

    public VoxelShape getShape(Direction dir){
        switch(dir){
            case EAST: return east;
            case SOUTH: return south;
            case WEST: return west;
            default: return this.north;
        }
    }

}
