package net.spectre.pirates.blocks;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.spectre.pirates.Pirates;

public class ModBlocks {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(Block.class, Pirates.MODID);

    public static final RegistryObject<CannonBlock> CANNON = BLOCKS.register("cannon", () -> new CannonBlock(AbstractBlock.Properties.of(Material.HEAVY_METAL)));
}
